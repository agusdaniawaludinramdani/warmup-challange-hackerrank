/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warmup.challange.hackerrank;

import java.util.Scanner;

/**
 *
 * @author dans
 */
public class MiniMaxSum {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] arr = new int[5];
        for(int arr_i = 0; arr_i < 5; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        
        long minValue = 0;
        long maxValue = 0;
        
        for(int i = 0; i < 5; i++){
            long minV = sumOfNumbers(i, arr);            
            long maxV = sumOfNumbers(i, arr);
            
            if(i == 0){
                minValue = minV;
                maxValue = maxV;
            }
            
            if(minV < minValue){
                minValue = minV;
            }
            
            if(maxV > maxValue){
                maxValue = maxV;
            }
        }
        System.out.println(minValue + " " + maxValue);
        
        in.close();
    }
    
    static long sumOfNumbers(int a, int[] arr){
        long sum = 0;
        for(int i = 0; i < 5; i++){
            if(i != a){
                sum += arr[i];
            }
        }
        return sum;
    }

}
