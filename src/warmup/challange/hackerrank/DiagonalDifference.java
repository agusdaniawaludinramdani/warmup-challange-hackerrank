/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warmup.challange.hackerrank;

import java.util.Scanner;

/**
 *
 * @author dans
 */
public class DiagonalDifference {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[][] a = new int[n][n];
        int rightDiagonal = 0;
        int leftDiagonal = 0;

        for (int a_i = 0; a_i < n; a_i++) {
            for (int a_j = 0; a_j < n; a_j++) {
                a[a_i][a_j] = in.nextInt();
                if (a_i == a_j) {
                    rightDiagonal = rightDiagonal + a[a_i][a_j];
                }
                if (a_i + a_j == n - 1) {
                    leftDiagonal = leftDiagonal + a[a_i][a_j];
                }
            }
        }
        System.out.println(Math.abs(rightDiagonal - leftDiagonal));
        in.close();
    }

}
