/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warmup.challange.hackerrank;

import java.util.Scanner;

/**
 *
 * @author dans
 */
public class BirthDayCakeCandles {

    static int birthdayCakeCandles(int n, int[] ar) {
        int result = 0;
        int greaterThan = 0;

        for (int i = 0; i < ar.length; i++) {
            if (ar[i] > greaterThan) {
                greaterThan = ar[i];
            }
        }

        for (int i = 0; i < ar.length; i++) {
            if (ar[i] == greaterThan) {
                result++;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] ar = new int[n];
        for (int ar_i = 0; ar_i < n; ar_i++) {
            ar[ar_i] = in.nextInt();
        }
        int result = birthdayCakeCandles(n, ar);
        System.out.println(result);
    }
}
