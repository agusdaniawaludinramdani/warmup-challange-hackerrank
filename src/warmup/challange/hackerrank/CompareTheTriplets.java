/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warmup.challange.hackerrank;

import java.util.Scanner;

/**
 *
 * @author dans
 */
public class CompareTheTriplets {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] alicePoint = new int[3];
        int[] bobPoint = new int[3];
        int aliceResult = 0;
        int bobResult = 0;

        for (int i = 0; i < 3; i++) {
            alicePoint[i] = in.nextInt();
        }

        for (int i = 0; i < 3; i++) {
            bobPoint[i] = in.nextInt();
        }

        for (int i = 0; i < 3; i++) {
            if (alicePoint[i] > bobPoint[i]) {
                aliceResult++;
            } else if (alicePoint[i] < bobPoint[i]) {
                bobResult++;
            }
        }
        in.close();
        System.out.print(aliceResult + " " + bobResult);

    }
}
