/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warmup.challange.hackerrank;

import java.util.Scanner;

/**
 *
 * @author dans
 */
public class TimeConversion {

    static String timeConversion(String s) {
        String listTime[] = s.split(":");
        String hour = listTime[0];
        String minute = listTime[1];
        String second = listTime[2].substring(0, 2);
        String caser = listTime[2].substring(2, 4);

        if (caser.equals("AM")) {
            if (hour.equals("12")) {
                hour = "00";
            }
            return hour + ":" + minute + ":" + second;
        } else {
            if (!hour.equals("12")) {
                int h = Integer.parseInt(hour);
                h = h + 12;
                hour = "" + h;
            }
            return hour + ":" + minute + ":" + second;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        String result = timeConversion(s);
        System.out.println(result);
    }
}
