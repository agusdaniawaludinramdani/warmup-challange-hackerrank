/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warmup.challange.hackerrank;

import java.util.Scanner;

/**
 *
 * @author dans
 */
public class PlusMinus {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        int posResult = 0;
        int negResult = 0;
        int zerResult = 0;
        for (int arr_i = 0; arr_i < n; arr_i++) {
            arr[arr_i] = in.nextInt();
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                posResult++;
            } else if (arr[i] < 0) {
                negResult++;
            } else if (arr[i] == 0) {
                zerResult++;
            }
        }
        System.out.printf("%.6f \n", (float) posResult / n);
        System.out.printf("%.6f \n", (float) negResult / n);
        System.out.printf("%.6f \n", (float) zerResult / n);
        in.close();
    }
}
